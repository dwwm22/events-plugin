<?php
/*
Plugin Name: Events-Plugin
Description: Le meilleur allié de vos événements
Version: 1.0
Author : Sabrina
*/

//Sécurité si jamais on essaye d'accéder autrement à la page
if (!defined('ABSPATH')) {
    exit; //Exit if accessed directly
}

//Verification que les deux plugins soient actifs
include_once(ABSPATH . 'wp-admin/includes/plugin.php');

if (is_plugin_active('advanced-custom-fields/acf.php') && is_plugin_active('woocommerce/woocommerce.php')) {
    // plugin is active

    //Ajout du hook pour exécuter ma fonction
    add_action('acf/init', 'events_my_acf_init');

    function events_my_acf_init()
    {
        //Fonction qui ne se fera que si la fonction existe (donc si le plugin est actif)
        if (function_exists('acf_add_local_field_group')):

            //Ajout des champs ACF
            acf_add_local_field_group(
                array(
                    'key' => 'group_1',
                    'title' => 'Détails de l\'événement',
                    'fields' => array(
                        array(
                            'key' => 'field_1',
                            'label' => 'Date de l\'événement',
                            'name' => 'date_event',
                            'type' => 'date_picker',
                        ),
                        array(
                            'key' => 'field_2',
                            'label' => 'Heure de l\'événement',
                            'name' => 'hour_event',
                            'type' => 'time_picker',
                            'display_format' => 'H:i',
                            'return_format' => 'H:i',
                        ),
                        array(
                            'key' => 'field_3',
                            'label' => 'Description',
                            'name' => 'description_event',
                            'type' => 'text',
                        ),
                        array(
                            'key' => 'field_4',
                            'label' => 'Informations privées',
                            'name' => 'private_event',
                            'type' => 'text',
                        )
                    ),
                    'location' => array(
                        array(
                            array(
                                'param' => 'post_type',
                                'operator' => '==',
                                'value' => 'product',
                            ),
                        ),

                    ),
                ),

            );

        endif;
    }

    //Code pour affichage du CSS (fond de couleur bleue dans description)
    function events_css_enqueue_style()
    {
        wp_enqueue_style('style', plugins_url('css/styles.css', __FILE__));
    }

    //Appel du hook des scripts
    add_action('wp_enqueue_scripts', 'events_css_enqueue_style');


    //Codes pour Shortcode pour l'affichage côté front

    function events_shortcode($attrs)
    {
        $event = '<h2>Détails de l\'événement</h2>
    <p>Date(s) : ' . get_field('date_event') . ' </p>
    <p>Heure : ' . get_field('hour_event') . ' </p>
    <p>Description : ' . get_field('description_event') . ' </p>
    
    <h3>Temps restant avant l\'événement</h3>
    <div id="demo"> </div>

    <h3>Informations Supplémetaires :</h3>
    <p> ' . get_field('private_event') . '</p>';



        return $event;
    }
    //Appel du shortcode de la fonction
    add_shortcode('events', 'events_shortcode');


    //Code bonus pour compteur
    //Ajout du hook et de la fonction pour appeler le fichier js
    function events_js_enqueue_scripts()
    {
        wp_enqueue_script('script', plugins_url('js/script.js', __FILE__), array('jquery'), '', true);
    }

    //Appel du hook des scripts
    add_action('wp_enqueue_scripts', 'events_js_enqueue_scripts');

}









